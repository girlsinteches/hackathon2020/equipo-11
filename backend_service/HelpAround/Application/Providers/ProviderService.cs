﻿using Application.Interfaces;
using Domain.Common;
using Domain.Dependants;
using Domain.Products;
using Domain.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Application.Providers
{
    public class ProviderService : IProviderService
    {
        private IDatabaseService _databaseService;
        public ProviderService(IDatabaseService databaseService)
        {
            _databaseService = databaseService;
        }

        public Provider Add(Provider provider)
        {
            _databaseService.Provider.Add(provider);
            _databaseService.Save();

            return provider;
        }

        public List<Provider> GetAll()
        {
            return _databaseService.Provider.ToList();
        }

        public Provider Get(long id)
        {
            var provider = _databaseService.Provider.Single(a => a.ProviderId == id);
            return provider;
        }

        public Provider Update(Provider provider)
        {
            _databaseService.Provider.Update(provider);
            _databaseService.Save();

            return provider;
        }
    }
}
