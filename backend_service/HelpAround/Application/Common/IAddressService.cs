﻿using Domain.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Common
{
    public interface IAddressService
    {
        public List<Address> GetAll();
        public Address Get(long id);
        public Address Add(Address address);
        public Address Update(Address address);
    }
}
