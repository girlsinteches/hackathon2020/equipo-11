﻿using Domain.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Common
{
    public interface ILocalityService
    {
        public List<Locality> GetAll();
        public Locality Get(long id);
        public Locality Add(Locality locality);
        public Locality Update(Locality locality);
    }
}
