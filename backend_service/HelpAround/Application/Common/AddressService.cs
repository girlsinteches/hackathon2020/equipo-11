﻿using Application.Interfaces;
using Domain.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Application.Common
{
    public class AddressService: IAddressService
    {
        private IDatabaseService _databaseService;
        public AddressService(IDatabaseService databaseService)
        {
            _databaseService = databaseService;
        }

        public Address Add(Address address)
        {
            _databaseService.Address.Add(address);
            _databaseService.Save();

            return address;
        }

        public List<Address> GetAll()
        {
            return _databaseService.Address.ToList();
        }

        public Address Get(long id)
        {
            var address = _databaseService.Address.Single(a => a.AddressId == id);
            return address;
        }

        public Address Update(Address address)
        {
            _databaseService.Address.Update(address);
            _databaseService.Save();

            return address;
        }
    }
}
