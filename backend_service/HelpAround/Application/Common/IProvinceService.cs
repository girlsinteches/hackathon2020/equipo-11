﻿using Domain.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Common
{
    public interface IProvinceService
    {
        public List<Province> GetAll();
        public Province Get(long id);
        public Province Add(Province locality);
        public Province Update(Province locality);
    }
}
