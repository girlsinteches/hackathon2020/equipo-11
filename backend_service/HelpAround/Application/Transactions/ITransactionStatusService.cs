﻿using Domain.Providers;
using Domain.Transactions;
using System.Collections.Generic;

namespace Application.Transactions
{
    public interface ITransactionStatusService
    {
        public List<TransactionStatus> GetAll();
        public TransactionStatus Get(long id);
        public TransactionStatus Add(TransactionStatus transactionStatus);
        public TransactionStatus Update(TransactionStatus transactionStatus);
    }
}
