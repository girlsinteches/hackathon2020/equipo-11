﻿using Domain.Transactions;
using System.Collections.Generic;

namespace Application.Transactions
{
    public interface ITransactionDetailService
    {
        public List<TransactionDetail> GetAll();
        public TransactionDetail Get(long id);
        public TransactionDetail Add(TransactionDetail transactionDetail);
        public TransactionDetail Update(TransactionDetail transactionDetail);
    }
}
