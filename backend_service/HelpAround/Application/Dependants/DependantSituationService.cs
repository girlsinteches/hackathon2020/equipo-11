﻿using Application.Interfaces;
using Domain.Common;
using Domain.Dependants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Application.Dependants
{
    public class DependantSituationService : IDependantSituationService
    {
        private IDatabaseService _databaseService;
        public DependantSituationService(IDatabaseService databaseService)
        {
            _databaseService = databaseService;
        }

        public DependantSituation Add(DependantSituation dependantSituation)
        {
            _databaseService.DependantSituation.Add(dependantSituation);
            _databaseService.Save();

            return dependantSituation;
        }

        public List<DependantSituation> GetAll()
        {
            return _databaseService.DependantSituation.ToList();
        }

        public DependantSituation Get(long id)
        {
            var dependantSituation = _databaseService.DependantSituation.Single(a => Convert.ToInt64(a.DependantSituationId) == id);
            return dependantSituation;
        }

        public DependantSituation Update(DependantSituation dependantSituation)
        {
            _databaseService.DependantSituation.Update(dependantSituation);
            _databaseService.Save();

            return dependantSituation;
        }
    }
}
