﻿using Domain.Dependants;
using System.Collections.Generic;

namespace Application.Dependants
{
    public interface IDependantService
    {
        public List<Dependant> GetAll();
        public Dependant Get(long id);
        public Dependant Add(Dependant dependant);
        public Dependant Update(Dependant dependant);
    }
}
