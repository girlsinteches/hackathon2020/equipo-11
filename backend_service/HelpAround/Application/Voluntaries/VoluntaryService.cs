﻿using Application.Interfaces;
using Domain.Providers;
using Domain.Transactions;
using Domain.Voluntaries;
using System.Collections.Generic;
using System.Linq;

namespace Application.Voluntaries
{
    public class VoluntaryService : IVoluntaryService
    {
        private IDatabaseService _databaseService;
        public VoluntaryService(IDatabaseService databaseService)
        {
            _databaseService = databaseService;
        }

        public Voluntary Add(Voluntary voluntary)
        {
            _databaseService.Voluntary.Add(voluntary);
            _databaseService.Save();

            return voluntary;
        }

        public List<Voluntary> GetAll()
        {
            return _databaseService.Voluntary.ToList();
        }

        public Voluntary Get(long id)
        {
            var voluntary = _databaseService.Voluntary.Single(a => a.VoluntaryId == id);
            return voluntary;
        }

        public Voluntary Update(Voluntary voluntary)
        {
            _databaseService.Voluntary.Update(voluntary);
            _databaseService.Save();

            return voluntary;
        }
    }
}
