﻿using Domain.Common;

namespace Domain.Transactions
{
    public class TransactionStatus
    {
        public enum TransactionStatusEnum
        {
            Created = 1
            , OnShopping = 2
            , OnDelivery = 3
            , Delivered = 4
            , Canceled = 5
        }
        public TransactionStatusEnum TransactionStatusId { get; set; }
        public string Value { get; set; }
    }
}
