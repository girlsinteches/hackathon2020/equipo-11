﻿using Domain.Common;

namespace Domain.Dependants
{
    public class DependantSituation
    {
        public enum DependantSituationEnum
        {
            Quarantine = 1
            , Disabled = 2
            , MedicalReason = 3
        }
        public DependantSituationEnum DependantSituationId { get; set; }
        public string Value { get; set; }
    }
}
