﻿using Domain.Common;
using Domain.Transactions;
using System.Collections.Generic;

namespace Domain.Voluntaries
{
    public class Voluntary
    {
        public Voluntary()
        {
            Transactions = new List<Transaction>();
        }
        public long VoluntaryId { get; set; }
        public long ModelBaseId { get; set; }
        public ModelBase ModelBase { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public bool IsAvailable { get; set; }
        public List<Transaction> Transactions { get; set; }
    }
}
