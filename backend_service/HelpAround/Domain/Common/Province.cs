﻿using System.Collections.Generic;

namespace Domain.Common
{
    public class Province
    {
        public Province()
        {
            Localities = new List<Locality>();
            Addresses = new List<Address>();
        }
        public int ProvinceId { get; set; }
        public long ModelBaseId { get; set; }
        public ModelBase ModelBase { get; set; }
        public List<Locality> Localities { get; set; }
        public List<Address> Addresses { get; set; }
        public string Name { get; set; }
    }
}
