﻿using System;

namespace Domain.Common
{
    public class ModelBase
    {
        public long ModelBaseId { get; set; }
        public RecordStatusId RecordStatusId { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime? UpdateDate { get; set; }
    }
}
