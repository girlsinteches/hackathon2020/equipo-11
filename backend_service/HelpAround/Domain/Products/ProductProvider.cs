﻿using Domain.Providers;

namespace Domain.Products
{
    public class ProductProvider
    {
        public long ProductId { get; set; }
        public Product Product { get; set; }
        public long ProviderId { get; set; }
        public Provider Provider { get; set; }
    }
}
