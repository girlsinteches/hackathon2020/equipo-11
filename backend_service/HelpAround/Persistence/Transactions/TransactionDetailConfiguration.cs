﻿using Domain.Transactions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Persistence.Transactions
{
    public class TransactionDetailConfiguration : IEntityTypeConfiguration<TransactionDetail>
    {
        public void Configure(EntityTypeBuilder<TransactionDetail> builder)
        {
            builder.ToTable(nameof(TransactionDetail));
            builder.HasKey(t => t.TransactionDetailId);

            builder.HasOne(t => t.Product)
                .WithMany(t => t.TransactionDetails)
                .HasForeignKey(t => t.ProductId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(t => t.Provider)
                .WithMany(t => t.TransactionDetails)
                .HasForeignKey(t => t.ProviderId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(t => t.Transaction)
                .WithMany(t => t.TransactionDetails)
                .HasForeignKey(t => t.TransactionId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
