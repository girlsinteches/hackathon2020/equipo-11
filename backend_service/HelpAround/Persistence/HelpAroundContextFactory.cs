﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace Persistence
{
    public class HelpAroundContextFactory : IDesignTimeDbContextFactory<HelpAroundContext>
    {
        public HelpAroundContextFactory()
        {

        }
        private IConfiguration Configuration => new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json")
            .Build();
        public HelpAroundContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<HelpAroundContext>();
            builder.UseSqlServer(Configuration.GetConnectionString("HelpArround"));

            return new HelpAroundContext(builder.Options);
        }
    }
}
