﻿using Domain.Common;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Persistence.Common
{
    public class LocalityConfiguration : IEntityTypeConfiguration<Locality>
    {
        public void Configure(EntityTypeBuilder<Locality> builder)
        {
            builder.ToTable(nameof(Locality));
            builder.HasKey(t => t.LocalityId);

            builder.Property(t => t.Name)
                .IsRequired();

            builder.HasOne(t => t.ModelBase)
                .WithOne()
                .HasForeignKey<Locality>(t => t.ModelBaseId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(t => t.Province)
                .WithMany(t => t.Localities)
                .HasForeignKey(t => t.ProvinceId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
