﻿using Domain.Common;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Persistence.Common
{
    public class ModelBaseConfiguration : IEntityTypeConfiguration<ModelBase>
    {
        public void Configure(EntityTypeBuilder<ModelBase> builder)
        {
            builder.ToTable(nameof(ModelBase));
            builder.HasKey(t => t.ModelBaseId);

            builder.Property(e => e.RecordStatusId)
                .HasConversion<int>();
        }
    }
}
