﻿using Domain.Common;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Persistence.Common
{
    public class AddressConfiguration : IEntityTypeConfiguration<Address>
    {
        public void Configure(EntityTypeBuilder<Address> builder)
        {
            builder.ToTable(nameof(Address));
            builder.HasKey(t => t.AddressId);

            builder.Property(t => t.Street)
                .IsRequired()
                .HasMaxLength(200);

            builder.Property(t => t.Stairs)
                .HasMaxLength(200);

            builder.Property(t => t.Door)
                .HasMaxLength(200);

            builder.HasOne(t => t.ModelBase)
                .WithOne()
                .HasForeignKey<Address>(t => t.ModelBaseId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(t => t.Province)
                .WithMany(t => t.Addresses)
                .HasForeignKey(t => t.ProvinceId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(t => t.Locality)
                .WithMany(t => t.Addresses)
                .HasForeignKey(t => t.LocalityId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(t => t.Dependant)
                .WithMany(t => t.Addresses)
                .HasForeignKey(t => t.DependantId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
