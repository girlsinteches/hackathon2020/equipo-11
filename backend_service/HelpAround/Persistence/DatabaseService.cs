﻿using Application.Interfaces;
using Domain.Common;
using Domain.Dependants;
using Domain.Products;
using Domain.Providers;
using Domain.Transactions;
using Domain.Voluntaries;
using Microsoft.EntityFrameworkCore;

namespace Persistence
{
    public class DatabaseService : IDatabaseService
    {
        private HelpAroundContext _helpAroundContext;
        public DatabaseService()
        {
            _helpAroundContext = new HelpAroundContextFactory().CreateDbContext(null);
        }

        public DbSet<ModelBase> ModelBase {
            get => _helpAroundContext.ModelBase;
            set => _helpAroundContext.ModelBase = value;
        }
        public DbSet<Province> Province
        {
            get => _helpAroundContext.Province;
            set => _helpAroundContext.Province = value;
        }
        public DbSet<Locality> Locality
        {
            get => _helpAroundContext.Locality;
            set => _helpAroundContext.Locality = value;
        }
        public DbSet<Address> Address
        {
            get => _helpAroundContext.Address;
            set => _helpAroundContext.Address = value;
        }

        public DbSet<Dependant> Dependant
        {
            get => _helpAroundContext.Dependant;
            set => _helpAroundContext.Dependant = value;
        }
        public DbSet<DependantSituation> DependantSituation
        {
            get => _helpAroundContext.DependantSituation;
            set => _helpAroundContext.DependantSituation = value;
        }
        public DbSet<DependantType> DependantType
        {
            get => _helpAroundContext.DependantType;
            set => _helpAroundContext.DependantType = value;
        }
        public DbSet<Voluntary> Voluntary
        {
            get => _helpAroundContext.Voluntary;
            set => _helpAroundContext.Voluntary = value;
        }
        public DbSet<Provider> Provider
        {
            get => _helpAroundContext.Provider;
            set => _helpAroundContext.Provider = value;
        }
        public DbSet<Product> Product
        {
            get => _helpAroundContext.Product;
            set => _helpAroundContext.Product = value;
        }

        public DbSet<ProductProvider> ProductProvider
        {
            get => _helpAroundContext.ProductProvider;
            set => _helpAroundContext.ProductProvider = value;
        }

        public DbSet<Transaction> Transaction
        {
            get => _helpAroundContext.Transaction;
            set => _helpAroundContext.Transaction = value;
        }
        public DbSet<TransactionDetail> TransactionDetail
        {
            get => _helpAroundContext.TransactionDetail;
            set => _helpAroundContext.TransactionDetail = value;
        }
        public DbSet<TransactionStatus> TransactionStatus
        {
            get => _helpAroundContext.TransactionStatus;
            set => _helpAroundContext.TransactionStatus = value;
        }

        public void Save()
        {
            _helpAroundContext.SaveChanges();
        }
    }
}
