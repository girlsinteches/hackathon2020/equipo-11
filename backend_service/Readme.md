Stack tecnológico

* .Net core como framework
* C# como lenguaje de programación
* Visual Studio como ID
* Sql server como motor de base de datos

Arquitectura

La arquitectura usada para organizar el backend fue Domain Driven Design (DDD)

Capas

* Domain: Incluye las entidades de dominio.
* Application: Incluye la mayoría de los servicios necesarios para las tablas planteadas para el proyecto.
* Persistence: Incluye las configuraciones de las tablas definidas usando Entity Framework (CodeFirst).
* Api: Proyecto WebApi que incluye unos cuantos endpoints, ya que por tiempo no se pudo continuar con todos.

**NiceToHave: Incluir el uso de repositorios y unidades de trabajo, ya que por tiempo no se incluyó.

*********

El proyecto actualmente se encuentra desplegado en una cuenta personal de azure y los siguientes
Endpoints de listado están respondiendo:

* Get https://apphelparound.azurewebsites.net/api/transactionstatus
* Get https://apphelparound.azurewebsites.net/api/dependantsituation
* Get https://apphelparound.azurewebsites.net/api/dependanttype
* Get https://apphelparound.azurewebsites.net/api/locality

*********

* Para crear la base de datos en otro servidor:

    * Se deberían ubicar en el proyecto "Persistense"
    * Actualizar la cadena de conección en el fichero "appsettings.json"
    * Ejecutar el comando "update-database" en el package manager console
        * O también "dotnet ef database update" desde la CLI de .NET Core

* Para hacer que el proyecto Api consuma otro servidor de base de datos

    * Actualizar la cadena de conexión del proyecto api en el fichero "appsettings.json" del mismo.
    * Publicar el proyecto web

* Para publicar el api en otro servidor:

    * Click derecho en el proyecto web y click en "publish"
    * Completar los datos del servidor donde se desea publicar y darle publicar