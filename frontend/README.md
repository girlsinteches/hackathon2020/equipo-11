# Ángel Frontend service

### Hecho con ReactJS

# Dependencias

  - react-router-dom: 5.2.0
    - Necesario para facilitar el enrutado dentro de la aplicación
  - superagent: 6.1.0
    - Necesario para realizar las peticiones HTTP (un poco más fácil de utilizar que fetch)
  - react-google-maps: 9.4.5
    - Necesario para la utilización de Google Maps como componente dentro de React
  - recompose: 0.30.0
    - Necesario para facilitar la reutilización de componentes, se usa junto con 
        react-google-maps

# TODOs
- No hay variabilización en el build, las peticiones al graph_service están hardcoeadas
    para solucionarlo:
    ```javascript
  /**
      Sustituir: http://h4h2020-t11-graph-service.northeurope.cloudapp.azure.com:5000/markets
      Por: La URL que quieras tener para el servicio http://localhost:5000 si lo tienes en local
  */
    
    ```
- En el componente MapComponent utiliza una URL sin el token de Google que te permite
    lanzar Google Maps por Javascript, debes cambiar el parámetro "googleMapURL" con 
    tu url de Google Maps para que no aparezca que está en modo desarrollo, pendiente
    de almacenar en un fichero .env para la variabilización entre entornos

- Aplicar navegación entre pantalla de registro y login,  nottificar al usuario de los
    cambios, que han sido realizados con éxito o en el caso de error, notificar del
    error (en el servicio de grafos, usuario, email e id de usuario son campos únicos)
- Habilitar la obtención del ID de usuario por parte del API (necesario para enlazar las 
    dos BBDDs) requiere de interacción con el backend_service
- Seguir trabajando en la aplicación para mejorar UI/UX y utilizar las funciones del
    API y el graph_service
 
# Inicio rápido para desarrollo

Tras clonar el repositorio

```bash
cd frontend && npm install && npm start
```

NOTA: si al hacer npm install da error por el tema de permisos el propio script te dará
pistas sobre dónde tienes el directorio npm-global. Al estar hecho con create-react-app
necesita de paquetes de NodeJS globales (ahorra tiempo de configuración)

Si quieres lanzar y exponer el servicio, recomiendo usar el paquete de NPM serve

```bash
sudo npm install -g serve
cd frontend && npm install && npm run build
serve -s build -p 3000
```

- El argumento -p es el puerto donde lo expones, puedes cambiarlo a tu gusto
- El directorio build tendrá los ficheros preparados para un entorno de producción,
puedes desplegarlo en un servidor web (Apache/Nginx/IIS) pero tendrás que aplicar
configuración para que desde la raíz o el index.html redireccione todas las rutas al
fichero HTML

Ejemplo para fichero .htaccess para un servidor web Apache2 (necesita tener habilitado
el módulo rewrite)
 
```
<IfModule mod_rewrite.c>

  RewriteEngine On
  RewriteBase /
  RewriteRule ^index\.html$ - [L]
  RewriteCond %{REQUEST_FILENAME} !-f
  RewriteCond %{REQUEST_FILENAME} !-d
  RewriteCond %{REQUEST_FILENAME} !-l
  RewriteRule . /index.html [L]

</IfModule>
```

## Caso de uso
En el estado actual sólo se conecta con el servicio de grafos 
(necesitarás tenerlo activo en local o desplegado)

1. Hacer login (necesita un usuario existente en el servicio de grafos, no requiere contraseña)
2. En la pantalla de inicio (te redirecciona tras el login, sino con refrescar la página sirve)
    tendás el mapa global con las tiendas que estén en el graph_service y las tendrás posicionadas
    en el mapa
3. Cada tienda tiene su propia visualización en el mapa (reutilizando el componente) y al abrirla
    mostrará los productos si los tiene (vienen desde el graph_service)