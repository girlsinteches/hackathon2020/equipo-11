import './App.css';
import React from "react";
import Home from "./Views/Home/Home";
import {BrowserRouter, Switch, Route, Redirect} from "react-router-dom";
import Login from "./Views/Login/Login";
import Register from "./Views/Register/Register";
import Dashboard from "./Views/Dashboard/Dashboard";
import Markets from "./Views/Markets/Markets";
import Tasks from "./Views/Tasks/Tasks";

class App extends React.Component{
  constructor(props) {
    super(props);
    this.state = {
      session: null
    }
  }

  render() {
    return <BrowserRouter>
      <Switch>
        <Route path={"/"} exact component={Home} />
        <Route path={"/login"} exact component={Login} />
        <Route path={"/register"} exact component={Register} />
        <Route path={"/dashboard"} exact component={Dashboard} />
        <Route path={"/markets"} exact component={Markets} />
        <Route path={"/tasks"} exact component={Tasks} />
        <Route path={"/logout"} exact render={() => {
          window.localStorage.clear();
          return <Redirect to={"/"} />
        }}/>

      </Switch>
    </BrowserRouter>
  }
}

export default App;
