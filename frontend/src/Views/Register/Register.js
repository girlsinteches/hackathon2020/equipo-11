import React, {useState} from "react";
import {Link, Redirect, withRouter} from "react-router-dom";
import ajax from "superagent";

const Register = ({...props}) => {
    const [session, setSession] = useState(window.localStorage.getItem('session'));
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [role, setRole] = useState('');
    const [email, setEmail] =  useState('');

    if (session)
        return <Redirect to={"/dashboard"} />;
    function registerUser(e){
        e.preventDefault();
        const registerRequest = ajax.post('http://h4h2020-t11-graph-service.northeurope.cloudapp.azure.com:5000/user').send({
            username: username,
            email: email
        }).set('Content-Type', 'application/x-www-form-urlencoded').set('Access-Control-Allow-Origin', '*')


    }
    return <div className="container-fluid">
        <div className="card" style={{
            marginTop: '25vh',
            width: '50vw',
            marginLeft: 'auto',
            marginRight: 'auto'
        }}>
            <div className="card-body">
                <h5 className="card-title">Registro</h5>
                <div className="card-text">
                    <form onSubmit={registerUser}>
                        <div className="form-group">
                            <label htmlFor="username">Nombre de usuario </label>
                            <input type="text" className="form-control" id={"username"}
                                   placeholder="Nombre de usuario"
                                   value={username}
                                   onChange={(e) => {
                                       setUsername(e.target.value)
                                   }}/>
                            <small id="emailHelp" className="form-text text-muted">Puedes iniciar sesión con tu nombre de usuario</small>
                        </div>
                        <div className="form-group">
                            <label htmlFor="role">Rol</label>
                            <select className={'form-control'} name="role" id="role" value={role}
                            onChange={(e) => {
                                setRole(e.target.value)
                            }}>
                                <option>Selecciona un rol</option>
                                <option value="dependant">Dependiente</option>
                                <option value="volunteer">Voluntario</option>
                            </select>
                        </div>
                        <div className="form-group">
                            <label htmlFor="password">Contraseña</label>
                            <input type="Contraseña" className="form-control" id="password"
                                   placeholder="Introduce tu contraseña"
                                   value={password}
                                   onChange={(e) => {
                                       setPassword(e.target.value)
                                   }}/>
                        </div>
                        <button type="submit" className="btn btn-primary">Registro</button>
                        <Link to={"/login"} className="btn btn-primary" style={{marginLeft: '1rem'}}>Iniciar sesión
                        </Link>
                    </form>
                </div>
            </div>
        </div>
    </div>
};

export default withRouter(Register);