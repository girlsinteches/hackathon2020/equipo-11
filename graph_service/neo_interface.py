from neo4j import GraphDatabase, basic_auth, BoltDriver, Session, Result


class NodeMixin(object):
    base_query = "{path_operation} {node_path} RETURN {return_nodes} {return_args}"

    def get_nodes(self, node_prefix: str, node_name: str, limit: int = 25, offset: int = 0) -> str:
        return self.base_query.format(
            path_operation="MATCH",
            node_path="({node_prefix}:{node_name})".format(
                node_name=node_name,
                node_prefix=node_prefix
            ),
            return_nodes=node_prefix,
            return_args=""
        )


class UserNodeMixin(NodeMixin):
    username = None
    user_id = None
    email = None
    neo4j_id = None
    lat = None
    lon = None

    def create_user(self, username: str, user_id: int, email: str, lat: str, lon: str) -> str:
        node_prefix = "u"
        node_name = "User"
        build_node = """
        ({node_prefix}: {node_name} {{
            username: "{username}",
            user_id: {user_id},
            email: "{email}",
            lat: "{lat}",
            lon: "{lon}"
        }})
        """.format(
            node_prefix=node_prefix,
            node_name=node_name,
            username=username,
            user_id=user_id,
            email=email,
            lat=lat,
            lon=lon
        )
        return self.base_query.format(
            path_operation="MERGE",
            node_path=build_node,
            return_nodes=node_prefix,
            return_args=""
        )


class DependentNode(UserNodeMixin):
    covid = None
    covid_since = None

    def create_dependent(self):
        pass


class VolunteerNode(UserNodeMixin):
    active = None


class MarketNode(NodeMixin):
    name = None
    category = None
    lat = None
    lon = None


class ProductNode(NodeMixin):
    name = None
    price = None
    description = None


class TaskNode(NodeMixin):
    created_at = None


class NeoInterface(object):
    driver = None

    def __init__(self, neo4j_host: str, neo4j_port: int, neo4j_user: str, neo4j_password: str):
        self.driver = self.start_driver(neo4j_host, neo4j_port, neo4j_user, neo4j_password)

    @classmethod
    def start_driver(cls, neo4j_host: str, neo4j_port: int, neo4j_user: str, neo4j_password: str) -> BoltDriver:
        """
        Starts the Neo4J Driver required for creating sessions

        :param neo4j_host:
        :param neo4j_port:
        :param neo4j_user:
        :param neo4j_password:
        :return: BoltDriver
        """
        return GraphDatabase.driver(
            uri='bolt://{host}:{port}'.format(
                host=neo4j_host,
                port=neo4j_port
            ),
            auth=basic_auth(
                user=neo4j_user,
                password=neo4j_password
            )
        )

    @classmethod
    def get_session(cls, neo4j_driver: BoltDriver, **config) -> Session:
        """
        Starts a Neo4J Session over the driver, required for running queries
        over the Neo4J driver

        :param neo4j_driver:
        :param config:
        :return:
        """
        return neo4j_driver.session(**config)

    @classmethod
    def run_query(cls, neo4j_session: Session, query: str, debug: bool = False) -> Result:
        """
        Runs a Neo4J Query

        :param neo4j_session:
        :param query:
        :param debug:
        :return:
        """
        if debug:
            print("Neo4J Query:\n {query}".format(query=query))

        return neo4j_session.run(query)
